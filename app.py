from bottle import route, run, template


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/')
def hollo():
    return 'hello wold'


run(host='localhost', port=8080)